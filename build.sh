#

jekyll build 1>/dev/null 2>&1
tic=$(date +%s)
k2j=$(ipfs add -Q -r _site --cid-base base36)
qm=$(ipfs add -Q -r _site --cid-base base58btc)
kmu=$(ipfs cid format -f "%m" -b base36 $qm)
echo "--- # $0"
echo tic: $tic
echo qm: $qm
echo k2j: $k2j
echo kmu: $kmu
echo url: http://$kmu.ipfs.localhost:8080/
echo $tic: $qm >> qm.log
cat <<EOT > _data/ipfs.yml
--- # ipfs hashes
tic: $tic
qm: $qm
k2j: $k2j
kmu: $kmu
url: $url
EOT
jekyll build
